DELIMITER //
CREATE procedure sp_chall2()

begin
-- select * from challenge2_dataset;

create temporary table firsttemp
select distinct Date, (abs(Open-Close)) as 'Range'
from challenge2_dataset
order by 2 desc limit 3;

-- select * from firsttemp;

create temporary table secondtemp
select Date, max(high) as maxhigh
from challenge2_dataset
where Date in (select Date from firsttemp)
group by Date
order by 2 desc limit 3;

-- select * from secondtemp;

create temporary table thirdtemp
select challenge2_dataset.Date, Time
from challenge2_dataset
join secondtemp ON challenge2_dataset.Date=secondtemp.Date
where High= maxhigh;

-- select * from thirdtemp;

SELECT firsttemp.Date, round(firsttemp.Range,4) as 'Range', thirdtemp.Time as 'Time of Max Price'
from firsttemp JOIN thirdtemp on firsttemp.Date=thirdtemp.Date
order by 2 desc;

drop table firsttemp;
drop table secondtemp;
drop table thirdtemp;

end 
// DELIMITER ;