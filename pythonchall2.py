#!/usr/bin/python
import re
import sys

fileopenreg= open('opra_example_regression.log','r')
rerec=re.compile('Record Publish:')
retype=re.compile('Type: Trade')
revol=re.compile('wTradeVolume')
reprice=re.compile('wTradePrice')

recpub=typetrade=tradevolume=tradeprice=''

for line in fileopenreg:
    field = re.split('\s+',line)
    if rerec.search(line):
        recpub = line
        typetrade=tradevolume=tradeprice=''
    if retype.search(line):
        typetrade = line
    if recpub and typetrade:
        if revol.search(line):
            tradevolume = line
        if reprice.search(line):
            tradeprice = line
    if recpub and typetrade and tradevolume and tradeprice:
        recpub=re.sub('^Regression:','',recpub)
        typetrade=re.sub('^Regression:','',typetrade)
        tradevolume=re.sub('^Regression:','',tradevolume)
        tradeprice=re.sub('^Regression:','',tradeprice)
        print(recpub.rstrip('\n') + typetrade + tradevolume + tradeprice.rstrip('\n'))
        recpub=typetrade=tradevolume=tradeprice=''
fileopenreg.close()
