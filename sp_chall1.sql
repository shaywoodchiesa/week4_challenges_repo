DELIMITER //
CREATE procedure sp_chall1(startdate varchar(8), starttime varchar(5))
begin
			set @startdate = startdate;
            set @starttime = starttime;
            select @startdate:= date_format(str_to_date(concat(@startdate, @starttime),'%Y%m%d%H:%i'),'%Y%m%d%H%i');
			select @endtime:= date_format(date_add(str_to_date(@startdate, '%Y%m%d%H%i'), interval 5 hour),'%Y%m%d%H%i');

    select 	sum(Close*Volume)/sum(Volume) as VWAP, 
			date_format(str_to_date(@startdate,'%Y%m%d'),'%d/%m/%Y') as 'Date',
			concat('Start (', date_format(str_to_date(@startdate,'%Y%m%d%H%i'), '%H:%i'), ') - End (', date_format(str_to_date(@endtime,'%Y%m%d%H%i'),'%H:%i'), ')') as 'Interval'
            
            from apple_stocks_dataset
    where Date between @startdate and @endtime;
end 
// DELIMITER ;

